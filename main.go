package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/providers/nytimes"
	"gitlab.com/news42/news42-core/providers/theguardian"
	"gitlab.com/news42/news42-core/providers/thesun"
	"gopkg.in/alecthomas/kingpin.v2"
	"sync"
)

var (
	verbose           = kingpin.Flag("verbose", "Enable verbose output").Short('v').Bool()
	apiURL            = kingpin.Flag("api-url", "API host").Short('u').Required().URL()
	apiToken          = kingpin.Flag("api-token", "API authentication token").Required().String()
	nytimesSource     = kingpin.Flag("nytimes-source", "NY Times Source ID").Required().Int()
	theguardianSource = kingpin.Flag("theguardian-source", "The Guardian Source ID").Required().Int()
	thesunSource      = kingpin.Flag("thesun-source", "The Sun Source ID").Required().Int()
)

func main() {
	kingpin.Parse()
	log.SetLevel(log.InfoLevel)
	if *verbose {
		log.SetLevel(log.DebugLevel)
	}
	log.Debug("Initialized application")
	// TODO: Initialize scraping

	n := nytimes.New((*apiURL).String(), *apiToken, *nytimesSource)
	g := theguardian.New((*apiURL).String(), *apiToken, *theguardianSource)
	s := thesun.New((*apiURL).String(), *apiToken, *thesunSource)
	log.Warn("test")

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		n.PushAll()
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		g.PushAll()
		wg.Done()
	}()
	wg.Add(1)
	go func() {
		s.PushAll()
		wg.Done()
	}()
	wg.Wait()
}
