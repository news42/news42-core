package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/lunny/log"
	"github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/providers"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

const UserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " +
	"AppleWebKit/537.36 (KHTML, like Gecko) " +
	"Chrome/53.0.2785.143 " +
	"Safari/537.36"

type Client struct {
	Client  *http.Client
	Request *http.Request
}

func NewClient(url string) (Client, error) {
	c := &http.Client{}
	r, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return Client{}, err
	}
	// Set a custom UA
	r.Header.Set("User-Agent", UserAgent)
	r.Header.Set("Accept-Encoding", "gzip")

	return Client{Client: c, Request: r}, nil
}

type Helper struct{}

func (*Helper) Request(url string) (*http.Response, error) {
	logrus.Debug("Attempting to create req for ", url)
	c, err := NewClient(url)
	if err != nil {
		return nil, err
	}
	response, err := c.Client.Do(c.Request)
	if err != nil {
		return nil, err
	} else if response.StatusCode != 200 {
		return nil, errors.New(response.Status)
	}
	logrus.Debugf("Made request to %s with result code %v and err %+v", url, response.StatusCode, err)
	return response, nil
}

func (*Helper) RequestSuccess(url string) bool {
	c, errc := NewClient(url)
	r, err := c.Client.Do(c.Request)
	logrus.Debugf("Made request to %s with result code %v and err %+v", url, r.StatusCode, err)
	return err == nil && errc == nil && r.StatusCode == 200
}

// Replaces URL with none if string is invalid
func (*Helper) ParseURL(u string) (*url.URL) {
	x, _ := url.Parse(u)
	return x
}

func NewHelper() Helper { return Helper{} }

type APIHelper struct {
	URL   string `json:"url"`
	Token string `json:"token"`
}

func (w *APIHelper) CreateRequest(method, url string, content io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, content)
	if err != nil {
		log.Warn("Failed to create request: ", err)
		return nil, err
	}

	req.Header.Set("Authorization", "Token "+w.Token)
	req.Header.Set("Content-Type", "application/json")
	req.Close = true

	c := &http.Client{}
	return c.Do(req)
}

func (h *APIHelper) PushAPI(w interface{}, apiPath string) (*http.Response, error) {
	buffer, err := json.Marshal(&w)
	if err != nil {
		log.Warn("Failed to marshal: ", err)
		return nil, err
	}
	resp, err := h.CreateRequest("POST", h.URL+"/api/"+apiPath+"/", bytes.NewBuffer(buffer))
	if err != nil {
		log.Warn("Failed to push whisky to backend (client create): ", err)
		return nil, err
	}
	return resp, nil
}

func (base *APIHelper) PushAPIID(w interface{}, apiPath string) (providers.IDResponse, error) {
	resp, err := base.PushAPI(w, apiPath)

	if err != nil || resp.StatusCode != 201 /* 201 = created */ {
		log.Warn("Failed to make request to backend: ", err, resp.Status)
		output, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Warn("Failed to read return error: ", err)
		}
		log.Warn(string(output[:]))
		return providers.IDResponse{Status: resp.StatusCode}, nil
	}

	defer resp.Body.Close()

	raw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Warn("Failed to read response: ", err)
		return providers.IDResponse{Status: resp.StatusCode}, nil
	}

	var r providers.IDResponse
	if json.Unmarshal(raw, &r) != nil {
		log.Warn("Failed to marshal response: ", err, string(raw))
		return providers.IDResponse{Status: resp.StatusCode}, nil
	}
	r.Status = resp.StatusCode

	return r, nil
}

func (h *APIHelper) PushCategory(category string) (providers.IDResponse, error) {
	return h.PushAPIID(category, "categories")
}

func NewAPIHelper(u string, token string) *APIHelper { return &APIHelper{URL: u, Token: token} }
