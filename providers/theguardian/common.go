package theguardian

import (
	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/client"
)

type TheGuardian struct {
	Feed   *gofeed.Feed `json:"feed"` // Holds a copy of the RSS feed
	Helper *client.APIHelper
	Source int
}

func New(url string, token string, source int) TheGuardian {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL("https://www.theguardian.com/world/rss")
	if err != nil {
		log.Fatal(err)
	}
	return TheGuardian{Feed: feed, Helper: client.NewAPIHelper(url, token), Source: source}
}
