package nytimes

import (
	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/client"
)

type NYTimes struct {
	Feed   *gofeed.Feed `json:"feed"` // Holds a copy of the RSS feed
	Helper *client.APIHelper
	Source int
}

func New(url string, token string, source int) NYTimes {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL("http://rss.nytimes.com/services/xml/rss/nyt/World.xml")
	if err != nil {
		log.Fatal(err)
	}
	return NYTimes{Feed: feed, Helper: client.NewAPIHelper(url, token), Source: source}
}
