package thesun

import (
	"github.com/mmcdole/gofeed"
	log "github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/client"
)

type TheSun struct {
	Feed   *gofeed.Feed `json:"feed"` // Holds a copy of the RSS feed
	Helper *client.APIHelper
	Source int
}

func New(url string, token string, source int) TheSun {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL("https://www.thesundaily.my/rss/world")
	if err != nil {
		log.Fatal(err)
	}
	return TheSun{Feed: feed, Helper: client.NewAPIHelper(url, token), Source: source}
}
