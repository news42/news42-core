package thesun

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/lunny/log"
	"github.com/mmcdole/gofeed"
	"github.com/sirupsen/logrus"
	"gitlab.com/news42/news42-core/providers"
	"io/ioutil"
	"net/url"
	"regexp"
	"sync"
	"time"
)

// PushCategory pushes a specific category to the API and returns the ID
func (n *TheSun) PushCategory(category string) int {
	// Get a list of categories from the feed
	response, err := n.Helper.CreateRequest("GET", fmt.Sprintf("%v/api/categories/?name=%v", n.Helper.URL, url.PathEscape(category)), nil)

	if err != nil {
		log.Warn("Failed to make request to query: ", err)
		return -1
	}
	defer response.Body.Close()

	raw, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Warn("Failed to read body: ", err)
		return -1
	}
	var r providers.CountResponse
	if err := json.Unmarshal(raw, &r); err != nil {
		logrus.Warn("Failed to unmarshal body: ", err)
		return -1
	}

	if len(r) == 0 {
		// Create the data
		idResponse, err := n.Helper.PushAPIID(struct {
			Category string `json:"name"`
		}{Category: category}, "categories")
		if err != nil {
			logrus.Warn("Failed to push category: ", err)
			return -1
		}
		return idResponse.ID
	}

	// Exists, return ID
	return r[0].ID
}

func (t *TheSun) GetArticle(item *gofeed.Item) (*providers.Article, error) {
	logrus.Debug(item.Link)
	var reg = regexp.MustCompile(`<[^>]*>`)
	v := &providers.Article{Title: item.Title, Link: item.Link,
		Created: item.PublishedParsed,
		Text:    reg.ReplaceAllString(item.Description, "")}

	if len(v.Text) < 10 {
		return v, errors.New("Text is too short")
	}

	if item.Categories != nil {
		v.Categories = item.Categories
	}

	return v, nil
}

type APIArticle struct {
	Title      string `json:"title"`
	Created    string `json:"created"`
	Author     string `json:"author"`
	Text       string `json:"text"`
	ScrapedAt  string `json:"scraped_at"`
	Source     int    `json:"source"`
	Categories []int  `json:"categories"`
	URL        string `json:"link"`
}

func FormatTime(n time.Time) string {
	// YYYY-MM-DDThh:mm
	return n.Format("2006-01-02T15:04")
}

func (n *TheSun) PushArticle(item *providers.Article) (providers.IDResponse, error) {
	// Iterate over available Articles
	v := APIArticle{Text: item.Text, Title: item.Title, Created: FormatTime(*item.Created), ScrapedAt: FormatTime(time.Now()), Source: n.Source, URL: item.Link}
	logrus.Debugf("%+v", v)

	v.Categories = []int{} // In case there are no categories
	for _, category := range item.Categories {
		v.Categories = append(v.Categories, n.PushCategory(category))
		logrus.Debug("Pushed categories: ", v.Categories)
	}

	itemID, err := n.Helper.PushAPIID(v, "rawarticles")
	if err != nil {
		logrus.Warn("Failed to push article: ", err)
		return providers.IDResponse{}, err
	}
	return itemID, err
}

func (n *TheSun) PushAll() {
	var wg sync.WaitGroup
	for i, item := range n.Feed.Items {
		wg.Add(1)
		go func(group *sync.WaitGroup) {
			article, err := n.GetArticle(item)
			if err != nil {
				logrus.Warn(err)
				wg.Done()
				return
			}
			id, err := n.PushArticle(article)
			if err != nil {
				logrus.Warn(err)
			}
			logrus.Info("Pushed article with ID: ", id)
			wg.Done()
		}(&wg)
		if i%1 == 0 {
			wg.Wait()
		}
	}
}
