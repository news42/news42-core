package providers

import "time"

type Meta struct {
	URL  string `json:"url"`
	Name string `json:"name"`
}

type IDResponse struct {
	ID     int `json:"id"`
	Status int `json:"status"`
}

type CountResponse []IDResponse

type Article struct {
	Title      string     `json:"title"`
	Author     string     `json:"author"`
	Text       string     `json:"text"`
	Abstract   string     `json:"abstract"`
	Created    *time.Time `json:"created"`
	Link       string     `json:"link"`
	Categories []string   `json:"categories"`
}
